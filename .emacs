(ido-mode)

(require 'package)
(push '("marmalade" . "http://marmalade-repo.org/packages/")

      package-archives )
(push '("melpa" . "http://melpa.milkbox.net/packages/")
      package-archives)
  (package-initialize)

'(add-hook 'python-mode-hook 'jedi:setup)
'(setq jedi:complete-on-dot t)

(evil-mode)

(add-to-list 'load-path "~/.emacs.d/org-mode/lisp/")
(add-to-list 'load-path "~/.emacs.d/evil-surround/")
(load "evil-surround.el")
(evil-surround-mode 1)
(global-evil-surround-mode 1)

(add-to-list 'auto-mode-alist '("\\.m\\'" . octave-mode))
(add-to-list 'auto-mode-alist '("\\.R\\'" . javascript-mode))
